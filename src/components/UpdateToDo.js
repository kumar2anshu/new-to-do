import React, { Component } from 'react'
import Button from '@material-ui/core/Button';

import UpdateIcon from '@material-ui/icons/Update';
import { connect } from "react-redux"
import { Update } from "../actions/index"


export class UpdateToDo extends Component {
    handleUpdate = event => {
        event.preventDefault();
        const updatedItem = this.item.value;
        const data = {
            updatedItem
        }
        //console.log("Hello");
        console.log(data);
        this.props.dispatch(

            //{ type: "UPDATE_TODO", id: this.props.todo.id, data: data }
            Update(this.props.todo.id, data)

        )
    }
    render() {
        return (
            <div>
                <center>
                    <form action="" onSubmit={this.handleUpdate}>
                        <input required placeholder="Enter Your New To do item" type="text" ref={(input) => this.item = input} defaultValue={this.props.todo.item} />

                        <Button variant="contained" color="secondary" startIcon={<UpdateIcon />} type="submit">Update</Button>



                    </form>


                </center>


            </div>
        )
    }
}

export default connect()(UpdateToDo)
