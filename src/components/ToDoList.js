
import React, { Component } from 'react'
import { connect } from "react-redux";
import ToDo from "./ToDo"
import UpdateToDo from "./UpdateToDo"
import { Jumbotron } from "reactstrap"
import { Input } from "@material-ui/core"
import SearchIcon from '@material-ui/icons/Search';
export class ToDoList extends Component {
    state = {
        search: ""
    };

    onChange = (e) => {
        this.setState(
            {
                search: e.target.value
            }
        )
    }
    render() {
        const { search } = this.state;
        const filteredToDo = this.props.todos.filter(todo => {
            return todo.item.toLowerCase().indexOf(search.toLowerCase()) > -1;
        })

        return (
            <div className="ToDoList">

                <center>
                    <hr />
                    <div>
                        <SearchIcon fontSize="large" />  <Input label="Search Country" icon="search" onChange={this.onChange} type="text" placeholder="Search Your ToDo By Name" />
                    </div><br />
                </center>
                <Jumbotron>
                    <center>
                        <table>
                            <col width="500" />
                            <col width="150" />
                            <col width="150" />
                            <col width="150" />
                            <thead>
                                <tr>
                                    <th>
                                        To Do List
                            </th>
                                    <th>
                                        Action 1
                            </th>
                                    <th>
                                        Action 2

                                    </th>
                                    <th>
                                        Created At
                            </th>
                                </tr>

                            </thead>

                        </table>


                    </center>
                    {console.log(this.props.todos)}
                    {console.log(filteredToDo)}

                    {filteredToDo.map((todo) => (
                        <div key={todo.id}>
                            {
                                todo.editing ? <UpdateToDo key={todo.id} todo={todo} /> :
                                    <ToDo key={todo.id} todo={todo} />
                            }

                        </div>
                    ))}
                </Jumbotron>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        todos: state
    }
}
export default connect(mapStateToProps)(ToDoList);


