import React, { Component } from 'react'
import { connect } from "react-redux"
import { EditToDo, DeleteToDo } from "../actions/index"
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';



export class ToDo extends Component {
    render() {
        return (


            <div>

                <center>
                    <table>
                        <col width="500" />
                        <col width="150" />
                        <col width="150" />
                        <tbody>
                            <tr>
                                <td text-align left>
                                    {this.props.todo.item}
                                </td>
                                <td text-align right>
                                    {<Button variant="contained" color="primary" endIcon={<EditIcon />} onClick={() => this.props.dispatch(
                                        EditToDo(this.props.todo.id)//we want to use separate action created in action file
                                        //{ type: "EDIT_TODO", id: this.props.todo.id }
                                    )}>Edit</Button>}
                                </td>

                                <td>
                                    {<Button variant="contained"
                                        color="secondary" startIcon={<DeleteIcon />} onClick={() => this.props.dispatch(
                                            DeleteToDo(this.props.todo.id)
                                            //{ type: "DELETE_TODO", id: this.props.todo.id }
                                        )}>Delete </Button>}

                                </td>
                                <td>
                                    {this.props.todo.id.toLocaleString()}

                                </td>


                            </tr>

                        </tbody>


                    </table>



                </center>
            </div>
        )
    }
}

export default connect()(ToDo);



