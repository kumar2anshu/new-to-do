
import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { connect } from "react-redux"
import { AddToDo } from "../actions/index"


export class CreateToDo extends Component {
    notify = () => toast.success("Successfully Added!!");
    handleSubmit = event => {
        event.preventDefault();
        const item = this.item.value;
        const data = {
            id: new Date(),
            item,
            editing: false
        }
        //console.log(data);
        this.props.dispatch(
            //{ type: "ADD_TODO", data }

            AddToDo(data)

        );
        this.item.value = "";
        this.notify();
        // alert("Your to do is successfully added")



    }
    render() {

        return (
            <div className="Create">
                <center>
                    <h3>Add Your To Do </h3>
                    <form action="" onSubmit={this.handleSubmit}>
                        <ToastContainer position="top-left"
                            autoClose={1000}
                            hideProgressBar={true} />

                        <TextField className="TextField" required placeholder="Enter Your To do item" inputRef={(input) => this.item = input} />
                        {/*  <input required placeholder="Enter Your To do item" type="text" ref={(input) => this.item = input} />
*/}
                        <Button className="AddToDo" variant="contained" color="primary" endIcon={<AddCircleOutlineIcon />} type="submit" >
                            ADD

                        </Button>

                        {/*  <button className="AddToDo" type="submit">Add item</button>*/}



                    </form>
                </center>

            </div>
        )
    }
}

export default connect()(CreateToDo);
