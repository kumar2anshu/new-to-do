import React from 'react';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import VisibilityIcon from '@material-ui/icons/Visibility';
import BorderColorRoundedIcon from '@material-ui/icons/BorderColorRounded';
import { Link } from 'react-router-dom';
import {
    Navbar,
    NavbarBrand,
    Nav,
    NavItem,
} from 'reactstrap';
import '../App.css';

class ToDoNavbar extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
        return (
            < div>
                <Navbar light expand='lg' bg='dark' variant='dark' color='info'>
                    <NavbarBrand href='/'>
                        <BorderColorRoundedIcon /> TODO APPLICATION
                    </NavbarBrand>


                    <Nav className='ml-auto' navbar>
                        <NavItem>
                            <Button className="NavBarButton" variant="contained" color="info" endIcon={<AddIcon />}>
                                <Link className='list' to='/create'>
                                    CreateToDo
                    </Link>

                            </Button>

                        </NavItem>

                        <NavItem>
                            <Button className="NavbarButton" variant="contained" color="info" endIcon={<VisibilityIcon />}>
                                <Link className='list' to='/view' >
                                    <i className=''></i> View
                    </Link>

                            </Button>

                        </NavItem>




                    </Nav>

                </Navbar>
            </div >
        );

    }
}

export default ToDoNavbar;
