import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';


import App from './App';
import { createStore } from "redux"
import { Provider } from "react-redux"
import reducerFunction from "./reducers/reducerFunction"
const store = createStore(reducerFunction);



ReactDOM.render(
    <Provider store={store}>

        <App />
    </Provider>, document.getElementById('root'));
