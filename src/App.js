import React from 'react';
import './App.css';
import CreateToDo from './components/CreateToDo';
import { BrowserRouter as Router } from 'react-router-dom';
import ToDoList from './components/ToDoList';
import { Route } from "react-router-dom"
import ToDoNavbar from './components/ToDoNavbar';


const App = () => {
  return (
    <Router>
      <div>
        <ToDoNavbar />

        <Route exact path='/create' component={CreateToDo} />
        <Route exact path='/view' component={ToDoList} />


      </div>
    </Router>

  );
}

export default App;
