//import { AddToDo, EditToDo, DeleteToDo } from "../../actions/index"
const DeleteToDo = (id) => {
    return (
        {
            type: "DELETE_TODO",
            id
        }
    )
}

const EditToDo = (id) => {
    return {
        type: "EDIT_TODO",
        id
    }
}


const AddToDo = (data) => {
    return {
        type: "ADD_TODO",
        data

    }
}

const Update = (id, data) => {
    return {
        type: "UPDATE_TODO",
        id,
        data
    }
}

test("should set up delete to do action object", () => {

    const action = DeleteToDo("testid12");
    expect(action).toStrictEqual(
        {
            type: "DELETE_TODO",
            id: "testid12"
        }
    )
});


test("should set edit todo object", () => {

    const action = EditToDo("ed12id");
    expect(action).toStrictEqual(
        {
            type: "EDIT_TODO",
            id: "ed12id"
        }
    )
});

test("should set up update todo object ", () => {
    const action = Update("id123", { updatedItem: "new to do" });
    expect(action).toStrictEqual(
        {
            type: "UPDATE_TODO",
            id: "id123",
            data: {
                updatedItem: "new to do"
            }
        }
    )

})

test("should set add todo object", () => {
    const data = {
        id: "id123",
        editing: false,
        item: "walking"

    }
    const action = AddToDo(data);
    expect(action).toStrictEqual(
        {
            type: "ADD_TODO",
            data: {
                id: "id123",
                editing: false,
                item: "walking"
            }

        }

    )


})