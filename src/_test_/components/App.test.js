import React from "react"
import Enzyme, { mount, shallow } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import App from "../../App";
Enzyme.configure({ adapter: new Adapter() });
import renderer from "react-test-renderer"




//snapshot for main component App
describe("Snapshot", () => {

    it("capturing snapshot", () => {
        const renderedValue = renderer.create(<App />).toJSON();
        expect(renderedValue).toMatchSnapshot();

    })
});