import React from "react"
import Enzyme, { shallow } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import { CreateToDo } from "../../components/CreateToDo";
Enzyme.configure({ adapter: new Adapter() });
import renderer from "react-test-renderer"


describe("Testing CreateToDo componenet", () => {
    it("includes 1 div with class name Create", () => {
        const wrapper = shallow(<CreateToDo />);

        expect(wrapper.find("div.Create")).toHaveLength(1);


    });

    // it("check text field", () => {
    //     const wrapper = shallow(<CreateToDo />);
    //     expect(wrapper.find("TextField.TextField").equals(<TextField required placeholder="Enter Your To do item" inputRef={(input) => this.item = input} />)).toBe(true)
    // });

    // it("add button check", () => {
    //     const wrapper = shallow(<CreateToDo />);
    //     expect(wrapper.find("Button.AddToDo")).to.equal(1);
    // });
});

//snapshot for create to do page
describe("Snapshot", () => {

    it("capturing snapshot", () => {
        const renderedValue = renderer.create(<CreateToDo />).toJSON();
        expect(renderedValue).toMatchSnapshot();

    })
});
