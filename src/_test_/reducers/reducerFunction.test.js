import reducerFunction, { initialState } from "../../reducers/reducerFunction"

describe("reducer test", () => {

    //test for no initial state 
    test("reducer test for initial state", () => {
        expect(reducerFunction(undefined, {})).toEqual(initialState)



    });


    //test for add
    test("reducer test for ADD_TODO", () => {
        expect(reducerFunction(undefined, {
            type: "ADD_TODO",
            data: {
                item: "Walking",
                editing: false
            }
        })).toStrictEqual([
            {
                item: 'Walking',
                editing: false
            },
            ...initialState
        ])


    });


    //test for delete

    test("reducer test for DELETE_TODO", () => {
        expect(reducerFunction(
            [
                {
                    id: "1",
                    item: "Coding",
                    editing: false
                }
            ]

            , {
                type: "DELETE_TODO",
                id: "1"
            })).toStrictEqual([])


    });



    // test for Edit
    test("reducer test for EDIT_TODO", () => {
        expect(reducerFunction(
            [
                {
                    id: "1",
                    item: "Coding",
                    editing: false
                }
            ]

            , {
                id: "1",
                type: "EDIT_TODO",


            })).toStrictEqual([
                {
                    id: "1",
                    item: "Coding",
                    editing: true
                }

            ])


    });



    //test for update



    test("reducer test for UPDATE_TODO", () => {
        expect(reducerFunction(
            [
                {
                    id: "1",
                    item: "Coding",
                    editing: "true"

                }
            ]

            , {
                type: "UPDATE_TODO",
                id: "1",
                data: {
                    updatedItem: "Studying"
                }




            })).toEqual([
                {
                    id: "1",
                    item: "Studying",
                    editing: false

                }

            ])


    });







})

